# EyepieceCradles

Parametric 3D models of telescope eyepiece holders in OpenSCAD.

Add a layer of soft weather striping foam to the saddles to gently cradle your eyepieces during transport and storage.
