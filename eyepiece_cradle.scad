// eyepiece_cradle.scad
//
// Parametric cradle for telescope eyepieces

// The number of facets around a circle ( more makes things slower )
$fn = 128;

// epsilon to use to ensure tangent parts overlap in CSG
epsilon = 0.005;

// set the thickness of walls for the model
wall = 3.0;

// The width of the foam rubber strip you'll use
foam_w = 10.0;

// The height of the foam rubber strip you'll use
foam_h = 4.8;

// extra space between each end of eyepiece and end of holder
end_gap = 5.0;

// extra space on each side of the eyepiece so that fingers can always get it and lift
side_gap = 10.0;

// Eyepiece parameters
// a = Barrel Diameter - typically 1+1/4 inch i.e. ~32mm or 2 inches (~51mm)
// b = main body diameter
// c = Eye cup diameter
// d = length of barrel
// e = overall length of eyepiece
//
//                      c
//     a       b         |
//     |       |         v
//     v     __V________/|
//     ______|           |
//     |     |           |
//     |<-d->|           |
//  -->|_____|           |<-- e
//           |__________ |
//     ^       ^        \|
//     |       |         ^
//                       |
//
//
// Cradle Parameters
// f = distance from end of barrel to start of body cradle
//
//   _                    ___
//  | |                   | |
//  | |______             | |
//  |        |   ___      | |
//  |        |___|  |_____| |
//  |_______________________|
//
//     |<-- f -->|
//
//
//                     a     b     c     d     e     f               Description, label       
//                     0,    1,    2,    3,    4,    5,                        6,  7   
eyepiece_params =[[ 32.0, 42.5, 43.0, 22.0, 86.0, 30.0, "5mm Generic 1+1/4 inch", "5" ],
                  [ 32.0, 42.5, 43.0, 22.0, 86.0, 30.0, "9mm Generic 1+1/4 inch", "9" ],
                  [ 32.0, 34.2, 40.0, 20.0, 55.0, 15.0, "15mm Generic 1+1/4 inch", "15" ],
                  [ 32.0, 33.5, 40.0, 20.0, 82.0, 27.0, "26mm Generic 1+1/4 inch", "26" ],
                  [ 51.0, 55.0, 66.0, 30.0, 125.0, 45.0, "40mm Generic 1+1/4 inch", "40" ]];


module eyepiece_cradle( pars ) {
  a = pars[0];
  b = pars[1];
  c = pars[2];
  d = pars[3];
  e = pars[4];
  f = pars[5];
  label = pars[7];

  max_dia = max( a, b, c );

  cradle_h = ( max_dia / 2.0 ) + foam_h + wall;
  cradle_w = max_dia + ( 2.0 * foam_h ) + ( 2.0 * side_gap );
  cradle_l = e + ( 2.0 * end_gap ) + ( 2.0 * wall );

  // eyepiece center height
  eyepiece_h = cradle_h;

  difference(){
    union(){

      difference() {
        // main cube of holder
        cube([ cradle_w, cradle_l, cradle_h ]);

        // subtract out a bunch of space under the main body
	translate([ wall, wall + end_gap + d, wall])
          cube([ cradle_w - ( 2.0 * wall ), e + end_gap - d, cradle_h ]);

        // subtract out part of the sides for easy finger access to eyepiece
	translate([ -wall, wall + end_gap + d, cradle_h / 2.0 ])
	  cube([ 2 * cradle_w, e + end_gap - d, 2 * cradle_h ]);

        // subtract out a the cylinder for the barrel
	translate([ cradle_w / 2.0, wall, cradle_h ])
          rotate(v=[1,0,0], a=-90)
	    cylinder( d = ( a + ( 2.0 * foam_h )), h = end_gap + d + epsilon);

        // recessed label for eyepiece size
	translate([11.5, wall, eyepiece_h - 1.0 ])
	  rotate(v=[ 0, 0, 1 ], a=90)
	    linear_extrude( height=2.0, convexity=20 )
	      scale([ 0.95, 0.95, 0.95 ])
	        text(label);

      } // difference 1

      difference() {

        // volume for body cradle
        translate([ 0, wall + end_gap + d + f, 0 ])
	  cube([ cradle_w, foam_w + 1.0, cradle_h ]);

        // subtract cylinder for body
        translate([ cradle_w / 2.0, wall + end_gap + d + f - epsilon, cradle_h ])
	  rotate( v=[ 1, 0, 0 ], a=-90 )
	    cylinder( d = b + ( 2.0 * foam_h ), h = foam_w + 1.0 + ( 2 * epsilon ));

      } // difference 2

    } // union


  } // difference
};

eyepiece_cradle( eyepiece_params[0]);
translate([ 100, 0, 0 ]) eyepiece_cradle( eyepiece_params[1]);
translate([ 200, 0, 0 ]) eyepiece_cradle( eyepiece_params[2]);
translate([ 300, 0, 0 ]) eyepiece_cradle( eyepiece_params[3]);
translate([ 400, 0, 0 ]) eyepiece_cradle( eyepiece_params[4]);
